
import flask
from flask import Flask,request

app = Flask(__name__)

@app.route('/')
def index2():
    """ Displays the index page accessible at '/'
    """
    return flask.render_template('index2.html')
@app.route('/calculadora')
def calculadora():
    return flask.render_template('calculadora.html')

@app.route('/calculadora/resultado', methods=['POST'])
def resultado():
    if request.method == "post":
        p1=float (request.form["prob1"])
        p2=float (request.form["prob2"])
        resul = p1 + p2

    return flask.render_template('resultado.html', resulta=resul)





if __name__ == '__main__':
    app.run(debug='true')
